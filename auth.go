package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

type Profile struct {
	Username string `json:"username"`
	Fullname string `json:"fullname"`
}

type ProfileCtxKey struct{}

func ProfileFromContext(ctx context.Context) (Profile, bool) {
	p, ok := ctx.Value(ProfileCtxKey{}).(Profile)
	return p, ok
}

type APIClaims struct {
	Profile
	Expiration time.Time `json:"exp"`
}

func (p APIClaims) Valid() error {
	if p.Expiration.Before(time.Now()) {
		return errors.New("token has expired")
	}
	return nil
}

func tokenBySecret(secret []byte) func(jwt.Claims) (string, error) {
	return func(c jwt.Claims) (string, error) {
		t := jwt.New(jwt.SigningMethodHS512)
		t.Claims = c
		return t.SignedString(secret)
	}
}

func parseWithSecret(secret string) func(string, jwt.Claims) (*jwt.Token, error) {
	return func(token string, cl jwt.Claims) (*jwt.Token, error) {
		return jwt.ParseWithClaims(token, cl, func(token *jwt.Token) (interface{}, error) {
			// Don't forget to validate the alg is what you expect:
			if token.Method != jwt.SigningMethodHS512 {
				return nil, errors.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(secret), nil
		})
	}
}

func authMiddleware(parseToken func(string, jwt.Claims) (*jwt.Token, error)) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			bearer := r.Header.Get("Authorization")
			if len(bearer) > 7 && strings.ToUpper(bearer[0:6]) == "BEARER" {
				bearer = bearer[7:]
			}
			p := APIClaims{}
			t, err := parseToken(bearer, &p)
			if err != nil {
				log.Println("cannot parse token ", bearer, ": ", err)
				http.Error(w, http.StatusText(403), 403)
				return
			}

			if !t.Valid {
				log.Println("invalid token ", bearer)
				http.Error(w, http.StatusText(403), 403)
				return
			}

			ctx = context.WithValue(ctx, ProfileCtxKey{}, p.Profile)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

func (c *controller) authHandler(w http.ResponseWriter, r *http.Request) {

	type authReq struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	reqData := authReq{}
	err := json.NewDecoder(r.Body).Decode(&reqData)
	if err != nil {
		http.Error(w, "wrong data: "+err.Error(), http.StatusBadRequest)
	}
	reqData.Username = strings.ToLower(reqData.Username)

	prof := Profile{
		Username: reqData.Username,
	}

	token, err := c.tokenAuth(APIClaims{
		Profile:    prof,
		Expiration: time.Now().Add(time.Hour * 2),
	})

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Authorization", "Bearer "+token)
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(prof)
	if err != nil {
		http.Error(w, "cannot encode response: "+err.Error(), http.StatusInternalServerError)
	}
}
