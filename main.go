package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
)

type controller struct {
	tokenAuth func(jwt.Claims) (string, error)
}

type claimsFunc func(jwt.Claims) (string, error)

func newController(ta claimsFunc) controller {
	return controller{
		tokenAuth: ta,
	}
}

func main() {

	secret := "my super secret string"
	ctrl := newController(tokenBySecret([]byte(secret)))
	ctx := context.Background()

	srv := &http.Server{
		Addr:    ":3333",
		Handler: ctrl.newRouter(secret),
	}

	fmt.Printf("Starting server on %v\n", srv.Addr)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal("start server: ", err)
		}
	}()

	done := make(chan os.Signal, 1)
	signal.Notify(done,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	stop := <-done
	log.Println("signal", stop.String())

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
}

func publicHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("welcome anonymous"))
}
func privateHandler(w http.ResponseWriter, r *http.Request) {

	prof, ok := ProfileFromContext(r.Context())
	if !ok {
		log.Println("cannot get profile from context")
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
		return
	}

	w.Write([]byte("welcome " + prof.Username))
}

func (c *controller) newRouter(secret string) http.Handler {
	r := chi.NewRouter()

	// Protected routes
	r.Group(func(r chi.Router) {
		r.Use(authMiddleware(parseWithSecret(secret)))
		r.Get("/admin", privateHandler)
	})

	// Public routes
	r.Group(func(r chi.Router) {
		r.Post("/", c.authHandler)
		r.Get("/", publicHandler)
	})

	return r
}
