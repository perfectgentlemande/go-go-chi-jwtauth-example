# go-chi/jwtauth Sample project

For educational purposes

## Requests

### Public
- `GET /` returns `welcome anonymous` message
- `POST /` with body `{"username": "usrnm", "password": "psswd"}` returns bearer token in `Authorization` header

### Private
- `GET /admin` returns `welcome {username}` message if request contains valid token